﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9997847
{
    class Program
    {
        //COMP5002 Programming Milestone 3 - Ashton Lye - 9997847
        //Pizza Ordering Program
        static void Main(string[] args)
        {
            var menuSelect = " ";
            var enterDetails = " ";

            var pizzaList = new List<Pizzas> { };
            var drinkList = new List<Drinks> { };

            Console.WriteLine("Welcome to Ashton's Pizza Parlour!");
            Console.WriteLine("Would you like to make an order? Please enter yes or no:");
            menuSelect = Console.ReadLine();

            if (menuSelect.ToLower() == "yes")
            {
                Console.WriteLine("");
                Console.WriteLine("Would you like to enter your details now? If so, enter yes, or enter no to enter your details later.");
                enterDetails = Console.ReadLine();

                //var enterDetails gets saved and passed down to the OrderReview method so we can check if the user has entered details yet.
                if (enterDetails.ToLower() == "yes")
                {
                    GetUserDetails();
                    OrderPizza(pizzaList, drinkList, enterDetails);                    
                }
                else
                {
                    OrderPizza(pizzaList, drinkList, enterDetails);
                }
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("Thanks for shopping with Ashton's Pizza!");
                Console.WriteLine("Press any key to exit");
                Console.ReadLine();
                //ends the program - user said they didn't want anything.
            }
        }

        //Method to get user's name and contact number - uses class UserDetails.
        public static void GetUserDetails()
        {
            var changeDetails = "yes";
            var newUser = new UserDetails();
            var contact = 0;
            var validNum = 0;

            while (changeDetails.ToLower() == "yes")
            {
                Console.WriteLine("");
                Console.WriteLine("Please enter your details for your order now:");
                Console.WriteLine("");
                Console.WriteLine("Please enter your name:");
                newUser.userName = Console.ReadLine();

                Console.WriteLine("Please enter your contact phone number:");
                do
                {
                    if (int.TryParse(Console.ReadLine(), out contact))
                    {
                        newUser.userContact = contact;
                        validNum = 1;
                    }
                    else
                    {
                        Console.WriteLine("Please enter a valid contact phone number (numbers only)");
                    }
                } while (validNum == 0);

                Console.WriteLine("");
                Console.WriteLine("These are the details you entered:");
                Console.WriteLine($"Name: {newUser.userName}");
                Console.WriteLine($"Contact Number: {newUser.userContact}");

                Console.WriteLine("");
                Console.WriteLine("Do you want to change your details? Enter yes or no:");
                changeDetails = Console.ReadLine();
            }
        }

        //Method to order pizzas and add info to class
        public static void OrderPizza(List<Pizzas> pizzaList, List<Drinks> drinkList, string enterDetails)
        {
            var pizzaSelect = 0;
            var sizeSelect = "";
            var i = 0;
            var orderDrink = "";
            var newPizza = new Pizzas();

            Console.Clear();
            Console.WriteLine("Here are the Pizzas we have available:");
            Console.WriteLine("");

            //prints the contents of the dictionary - the types of pizzas available. Doing it this way means I only need to add new pizzas to the dictionary to add new ones.
            for (i = 0; i <= 5; i++)
            {
                Console.WriteLine($"{i + 1}. {Pizzas.PizzaDict.ElementAt(i).Value}");
            }

            Console.WriteLine("");
            Console.WriteLine("Please enter the number corresponding to the Pizza you want to order:");
            do
            {
                if (int.TryParse(Console.ReadLine(), out pizzaSelect))
                {
                    //checking if the number they entered actually relates to a pizza - choose size if it does.
                    if (Pizzas.PizzaDict.ContainsKey(pizzaSelect))
                    {
                        newPizza.PizzaName = Pizzas.PizzaDict[pizzaSelect];
                        Console.WriteLine("");
                        Console.WriteLine("What size did you want? Enter the number of the size you want:");
                        Console.WriteLine("1. Regular");
                        Console.WriteLine("2. Large");
                        do
                        {
                            sizeSelect = Console.ReadLine();
                           
                            if (sizeSelect == "1")
                            {
                                newPizza.SetSize("Regular");
                                pizzaList.Add(newPizza);
                            }
                            else if (sizeSelect == "2")
                            {
                                newPizza.SetSize("Large");
                                pizzaList.Add(newPizza);
                            }
                            else
                            {
                                Console.WriteLine("Please enter a valid size selection.");
                            }
                            
                        } while (sizeSelect != "1" && sizeSelect != "2");    
                    }
                    else
                    {
                        Console.WriteLine($"Please enter a valid selection - a number between 1 and {Pizzas.PizzaDict.Count()}");
                    }       
                }
                else
                {
                    Console.WriteLine("Please enter a valid number.");
                }
            } while (pizzaSelect > Pizzas.PizzaDict.Count());
            //using PizzaDict.Count again lets me add new pizzas easily.

            Console.WriteLine("");
            Console.WriteLine("Would you like to order a Drink? Enter yes or no:");
            orderDrink = Console.ReadLine();
            if (orderDrink.ToLower() == "yes")
            {
                OrderDrinks(pizzaList, drinkList, enterDetails);
            }
            else
            {
                OrderReview(pizzaList, drinkList, enterDetails);
            }
        }

        //Method for ordering drinks and adding drink info to class
        public static void OrderDrinks(List<Pizzas> pizzaList, List<Drinks> drinkList, string enterDetails)
        {
            var userDrink = 0;
            var newDrink = new Drinks();

            Console.Clear();
            Console.WriteLine("Here are the Drinks we have available:");
            Console.WriteLine("");

            //prints the contents of the drinks dictionary - the types of drinks available. As with the pizzas, doing it this way means I only need to add new drinks to the dictionary to add new ones.
            for (var i = 0;  i < Drinks.DrinkDict.Count(); i++)
            {
                Console.WriteLine($"{i + 1}. {Drinks.DrinkDict.ElementAt(i).Value}");
            }

            Console.WriteLine("");
            Console.WriteLine("Please enter the number corresponding to the Drink you want to order:");
            do
            {
                //checking if the number they entered actually relates to a drink - add it to drinkList if it does.
                if (int.TryParse(Console.ReadLine(), out userDrink))
                {
                    if (Drinks.DrinkDict.ContainsKey(userDrink))
                    {
                        newDrink.DrinkName = Drinks.DrinkDict[userDrink];
                        drinkList.Add(newDrink);
                        OrderReview(pizzaList, drinkList, enterDetails);
                    }
                    else
                    {
                        Console.WriteLine($"Please enter a valid selection - a number between 1 and {Drinks.DrinkDict.Count()}");
                    }
                }
                else
                {
                    Console.WriteLine("Please enter a valid number.");
                }
            }while (userDrink > Drinks.DrinkDict.Count());
            //as before, using DrinkDict.Count lets me add new drinks easily.
        }

        //Method to display user's order
        public static void OrderReview(List<Pizzas> pizzaList, List<Drinks> drinkList, string enterDetails)
        {
            var addToOrder = "";
            var addType = "";
            var pTotal = 0.0;
            var dTotal = 0.0;
            double total = 0.0;

            Console.Clear();
            Console.WriteLine("These are the Pizzas you have ordered:");
            Console.WriteLine("");

            //printing the size, name and price of all the pizzas that have been ordered.
            for (var i = 0; i < pizzaList.Count(); i++)
            {
                Console.WriteLine($" - {pizzaList[i].PizzaSize} {pizzaList[i].PizzaName}, ${pizzaList[i].PizzaPrice.ToString("F")}");
                pTotal = pTotal + pizzaList[i].PizzaPrice;
            }

            //printing the name and price of all the drinks that have been ordered if any have been.
            if (drinkList.Count() > 0)
            {
                Console.WriteLine("");
                Console.WriteLine("These are the Drinks you have ordered:");
                Console.WriteLine("");

                for (var i = 0; i < drinkList.Count(); i++)
                {
                    Console.WriteLine($" - {drinkList[i].DrinkName}, ${drinkList[i].DrinkPrice.ToString("F")}");
                    dTotal = dTotal + drinkList[i].DrinkPrice;
                }
            }

            total = pTotal + dTotal;
            Console.WriteLine("");
            Console.WriteLine($"The total cost of your order is ${total.ToString("F")}.");
            Console.WriteLine("Would you like to add anything else to your order? Enter yes or no:");
            addToOrder = Console.ReadLine();

            //if the user wants to add things to their order, they whether to add pizza or drinks and the appropriate method is called. Yay modular code!
            if (addToOrder.ToLower() == "yes")
            {
                Console.WriteLine("");
                Console.WriteLine("What would you like to add?");
                Console.WriteLine("1. Pizzas");
                Console.WriteLine("2. Drinks");
                do
                {
                    addType = Console.ReadLine();
                    if (addType == "1")
                    {
                        OrderPizza(pizzaList, drinkList, enterDetails);
                    }
                    else if (addType == "2")
                    {
                        OrderDrinks(pizzaList, drinkList, enterDetails);
                    }
                    else
                    {
                        Console.WriteLine("Please enter a valid answer");
                    }
                } while (addType != "1" && addType != "2");
                
            }
            //if the user doesn't want anything else, check if they've already entered their details. If they have go straight to payment, otherwise get their details and then go to payment.
            else
            {
                if (enterDetails == "yes")
                {
                    Payment(total);
                }
                else
                {
                    GetUserDetails();
                    Payment(total);
                }
            }
        }

        //Method to get the user's payment
        public static void Payment(double total)
        {
            var cashPaid = 0.00;
            var changeGiven = 0.00;

            Console.WriteLine("");
            Console.WriteLine($"The total cost of your order is ${total.ToString("F")}");
            Console.WriteLine("");
            Console.WriteLine("We can only accept Cash payment. Please enter the amount of cash you have with no $ sign, e.g 5.50");
            do
            {
                if (double.TryParse(Console.ReadLine(), out cashPaid))
                {
                    if (cashPaid < total)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("Whoops! That isn't quite enough. Please add more to your payment:");
                    }
                    else
                    {
                        changeGiven = cashPaid - total;
                        Console.WriteLine("");
                        Console.WriteLine("Thank you for your payment.");
                        Console.WriteLine($"You have been given ${changeGiven.ToString("F")} in change");
                        Console.WriteLine("");
                        Console.WriteLine("Thank you for shopping at Ashton's Pizza Parlour!");
                        Console.WriteLine("Please come again!");
                        Console.WriteLine("");
                        Console.WriteLine("Press any key to exit the Program");
                        Console.ReadLine();
                        //ends program
                    }
                }
                else
                {
                    Console.WriteLine("Please enter a valid amount of money, with no $ sign, e.g $5.50 would be 5.50");
                }
            } while (cashPaid < total);       
            
            //the end :)
        }
    }
}
