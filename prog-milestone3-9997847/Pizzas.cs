﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9997847
{
    //class for storing pizza information.
    class Pizzas
    {
        public string PizzaName { get; set; }

        public string PizzaSize { get; set; }

        public double PizzaPrice { get; set; }

        //dictionary containing the available pizzas - code is written in such a way that I only have to remove/add pizzas to the dictionary to make them able to be ordered.
        public static Dictionary<int, string> PizzaDict = new Dictionary<int, string>
        {
            {1, "Hawaiian"},
            {2, "Meat Lovers"},
            {3, "Vegetarian"},
            {4, "Margarita"},
            {5, "Pepperoni"},
            {6, "Chicken & Mushroom"},
        };

        public void PizzaInfo(string pName, string pSize, double pPrice)
        {

            PizzaName = pName;
            PizzaSize = pSize;
            SetPrice(PizzaSize);
        }

        public void SetSize(string PizzaSize)
        {
            this.PizzaSize = PizzaSize;
            SetPrice(PizzaSize);                
        }

        private void SetPrice(string PizzaSize)
        {
            if (PizzaSize == "Regular")
            {
                PizzaPrice = 7.50;
            }
            else
            {
                PizzaPrice = 10.0;
            }
        }

    }
}
