﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9997847
{
    //class for storing drink information - probably unnecessary but why not. Keeps things uniform between drinks and pizzas anyway.
    class Drinks
    {
        public string DrinkName { get; set; }

        public double DrinkPrice = 5.00;

        //dictionary containing the available drinks - code is written in such a way that I only have to remove/add drinks to the dictionary to make them able to be ordered.
        public static Dictionary<int, string> DrinkDict = new Dictionary<int, string>
            {
                {1, "Coke"},
                {2, "Sprite"},
                {3, "Fanta"},
                {4, "L&P"},
            };

        public void DrinkInfo(string dName)
        {
            DrinkName = dName;
        }
    }
}
